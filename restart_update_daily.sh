#!/bin/sh

echo date > /home/server/restart.log;

cd /home/server/based.how;
git remote update;
read ahead behind <<< `git rev-list --left-right --count main...origin/main`;
if [[ "$behind" != "0" ]]; then
  echo "There are remote changes. Stopping gunicorn." >> /home/server/restart.log;
  
  service gunicorn stop;
  git pull >> /home/server/restart.log 2>&1;
  pip install -r requirements.txt >> /home/server/restart.log 2>&1;
  service gunicorn start;
  service gunicorn status >> /home/server/restart.log 2>&1;
fi

cd /home/server/articles.based.how;
git pull;