#!/usr/bin/env python

# Flask framework
from flask import Flask
from flask import render_template
from pyngrok import ngrok

# reading articles, creating database
import os, markdown, glob, sys, traceback, xxhash
import sqlite3
from contextlib import closing

# other components
import quotes

app = Flask(__name__)

DIR = os.path.dirname(__file__)
ARTICLES_FOLDER = os.path.abspath(
    os.path.join(DIR, os.pardir, os.pardir, 'articles.based.how'))
TEMPLATES_FOLDER = os.path.join(DIR, 'templates')
DB_FILE = os.path.abspath(
    os.path.join(DIR, os.pardir, 'articles.db'))
CERTS_DIR = os.path.join(os.path.expanduser('~'), 'cert')
MD_EXTENSIONS=['sane_lists', 'fenced_code', 'tables', 'abbr', 'attr_list', 'def_list']

@app.route("/")
def index():
    
    _db = db()
    with cur(_db) as c:
        titles = [x['title'] for x in c.execute(
            'select title from articles;')]
    with cur(_db) as c:
        tags = [x[0] for x in c.execute(
            'select distinct tag from tags;')]
    
    return render_template(
        'index.html', 
        articles=titles, 
        tags=tags, 
        quote=quotes.get_random())


@app.route("/articles/<title>")
def articles(title=''):
    
    _db = db()
    with cur(_db) as c:
        article = dict(c.execute(
            'select title, html from articles where title=?;',
            (title,)).fetchone())
    if article is None:
        web.not_found()
    
    with cur(_db) as c:
        tags = [x[0] for x in c.execute(
            'select tag from tags where article_title=?;', (title,))]
    
    return render_template(
        'article.html', 
        title=title, 
        tags=tags, 
        body=article['html'])


@app.route("/@<tag>")
def articles_by_tag(tag=''):
    
    with cur(db()) as c:
        titles = [x['article_title'] for x in c.execute(
            'select article_title from tags where tag=?;', [tag])]
    return render_template(
        'articles_by_tag.html', 
        articles=titles,
        tag=tag)


def db():
    _db = sqlite3.connect(DB_FILE, check_same_thread=False, uri=True)
    _db.row_factory = sqlite3.Row
    return _db


def cur(_db):
    return closing(_db.cursor())


def load_articles():
    print('Loading articles.')
    valuesArticles = []
    valuesTags = []
    _db = db()
    
    with cur(_db) as c:
        c.executescript('''
                create table if not exists articles (
                    title text primary key,
                    html blob,
                    hash text
                );
                
                create table if not exists tags (
                    article_title text, 
                    tag text
                );
            ''')
        _db.commit()
    
    if not os.path.exists(ARTICLES_FOLDER):
        print('Error: %s not found.'%ARTICLES_FOLDER) #TODO replace with logging library
    
    article_files = [f for f in os.listdir(ARTICLES_FOLDER) if f.endswith('.md')]
    if len(article_files) == 0:
        print('Error: %s contains no articles.'%ARTICLES_FOLDER)
    
    for article_file in article_files:
        title = article_file.replace('.md','').replace('_', ' ')
        article_path = os.path.join(ARTICLES_FOLDER, article_file)
        
        # avoid reading the same article into db.
        with open(article_path, 'r') as file:
            hash = xxhash.xxh64_hexdigest(file.read())
        with cur(_db) as c:
            isSame = c.execute(
                    'select 1 from articles where title=? and hash=?',
                    [title, hash]
                ).fetchone()
        if isSame:
            continue
        
        with open(article_path, 'r') as file:
            try:
                lines = file.readlines()
            except:
                print('Failure to read [%s] into database:'%article_file)
                traceback.print_exc()
                continue # can't read the file.
            
            tags = [t.strip() for t in lines.pop(0).replace('tags:','').split(',')]
            for tag in tags:
                valuesTags.append((tag, title))
            
            md = ''.join(lines)
            html = markdown.markdown( md, extensions=MD_EXTENSIONS )
            
            valuesArticles.append((html, title))
    
    with cur(_db) as c:
        c.executemany('''
            insert or ignore into articles 
            (html, title) 
            values (?,?)
            ''', valuesArticles)
        c.executemany('''
            update articles 
            set html=? 
            where title=?
            ''', valuesArticles)
        
        c.executemany('''
            insert or ignore into tags 
            (tag, article_title) 
            values (?,?)
            ''', valuesTags)
        c.executemany('''
            update tags 
            set tag=? 
            where article_title=?
            ''', valuesTags)
        
    _db.commit()



load_articles()

if __name__ == "__main__":
    app.run(debug=True, port=8080, host="0.0.0.0")