# Based.how

This site will be an elegant and concice collection of tutorials on based, 
*redpilled* or *arete* topics.

Inspired by based.cooking and fit.cooking


## Design

### Scope

The temporary scope of the project is to make a functional prototype with at 
most a few articles.


### Features

 - Tags, articles have 1 to many tags.
 - Uploading new articles.
 - [Markdown](https://www.markdownguide.org/basic-syntax) for use in articles.
 - A like, or comments system. (debatable for the prototype)

Articles are stored as directories with a markdown file + pictures used.

Articles may contain pictures, gifs, and short webms, articles may use embedded 
videos.


### Tech stack

[Pyramid](https://trypyramid.com/documentation.html) will be used to develop 
this web server.

Pyramid has support for, among others, the (chameleon)[https://docs.pylonsproject.org/projects/pyramid/en/latest/quick_tour.html?highlight=templating#templating]
templating engine, which will be used.

Lastly, the server deployment will be done through (nginx + pserve + supervisord)[https://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/deployment/nginx.html]

Aside rom this technology, unless there is a benefit to it, the website should 
be kept lean and simple.

html
css
jquery?
boostrap?
sass?
angular or some other stuff?

*The first version can be made manually with simple html and css*

*One concern is that over-bloated inelegant solutions should be avioded if 
possible*


### Data source (articles)

Each article is a markdown file.

The tags are added on the first line of the markdown file, in a comma separated 
list.

A git repo holds this project, however, articles should be in another one, to 
decouple data and code. This is the reason a folder based structure is ideal 
for this purpose.


## Howto

### Start the server on your machine

A step by step guide including the basics.

1. Ensure you have python, pip and venv installed, or a venv alternative.
    - `$ apt install python3 pip3`
    - `$ pip install venv`

2. Set up Gitlab and git by generating an ssh key and giving it to Gitlab.
    - `$ sudo apt install openssh` installs ssh if you don't have it (unlikely).
    - `$ cd ~/.ssh` go to your ssh folder.
    - `$ openssl genrsa -out based.how.pem 1024` generates the key pair.
    - `$ ssh-keygen -y -f based.how.pem > based.how.pub` extracts the private 
	key.
    - Upload the `based.how.pub` public key to https://gitlab.com/-/profile/keys

3. Pull this repository, and the articles repository.
    - `$ git clone git@gitlab.com:memetics_connected/based.how.git`
    - `$ git clone git@gitlab.com:memetics_connected/articles.based.how.git`
    - You may need to specify that a particular key has to be used: 
	`$ [command above] --config core.sshCommand="ssh -i your_ssh_folder/based.how.gitlab.pem"`

4. Setup the virtual environment for python.
    - `$ cd based.how`
    - `$ python -m venv venv` i like calling it venv or v, the second venv is 
	the name.
    - `$ venv/Scripts/activate` activates the virtual environment.
    - `(venv) $ pip install -r requirements.txt`

5. Start up the server
    - Repeat first three steps above.
    - `$ python src/server.py`
    - In your browser, go to: `localhost:8080`


### Handling Articles

Currently, articles are loaded in when the server starts.

A SQLite database is used for this.

The reason for this is the way python/web.py handles global variables/shared 
ram: We need the articles to be somewhere accessible to any thread. A database
is slightly more convenient than reading the files every time.

For now, SQLite is used for convenience. No ORM or other utilities are used yet
because there are only two tables, articles and tags.

Feel free to refacor database operations to its own file.